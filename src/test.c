#include "mem.h"
#include "mem_internals.h"
#include "test.h"
#include <stdio.h>

static void delete_heap(void* heap, size_t size) {
    munmap(heap, size_from_capacity((block_capacity){.bytes = size}).bytes);
}

static struct block_header* get_header(void* contents) {
    return (struct block_header*)((uint8_t*)contents - offsetof(struct block_header, contents));
}

void performTest(const char* testName, size_t heapSize, size_t* allocations, size_t numAllocations) {
    void* heap = heap_init(heapSize);

    if (heap == NULL) {
        printf("Не удалось инициализировать кучу для теста %s\n", testName);
        return;
    }

    debug_heap(stdout, heap);

    void* testAllocations[numAllocations];

    for (size_t i = 0; i < numAllocations; ++i) {
        testAllocations[i] = _malloc(allocations[i]);

        if (testAllocations[i] == NULL) {
            printf("Не удалось выделить память для теста %s (шаг %zu)\n", testName, i + 1);
            return;
        }
    }

    debug_heap(stdout, heap);

    for (size_t i = 0; i < numAllocations; ++i) {
        _free(testAllocations[i]);
    }

    debug_heap(stdout, heap);

    delete_heap(heap, heapSize);
    debug_heap(stdout, heap);
}

void first() {
    const size_t heapSize = 8192;
    performTest("first", heapSize, (size_t[]){4096}, 1);
}

void second() {
    const size_t heapSize = 8192;
    performTest("second", heapSize, (size_t[]){1024, 512}, 2);
}

void third() {
    const size_t heapSize = 8192;
    performTest("third", heapSize, (size_t[]){256, 128, 64}, 3);
}

void fourth() {
    const size_t heapSize = 8192;
    performTest("fourth", heapSize, (size_t[]){64, 8193}, 2);
}

void fifth() {
    const size_t heapSize = 1024;
    performTest("fifth", heapSize, (size_t[]){8193, 8193}, 2);
}
